#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  
#  Copyright 2019 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import parser
import pickle
import matplotlib.pylab as plt
from sklearn.decomposition import PCA
import numpy as np
from scipy.stats import spearmanr,pearsonr
from sklearn.cluster import KMeans,spectral_clustering,AgglomerativeClustering
import NN_models3_doppiabatch as NN_models
from sklearn.preprocessing import MinMaxScaler
class single_cell_renf:
	def __init__(self):
		self.scaler=MinMaxScaler()
		self.pca=PCA(n_components=3, copy=True, whiten=False, svd_solver="auto", tol=0.0, iterated_power="auto", random_state=None)
		
	def fit(self,diz_cells,death_functions):
		self.model=NN_models.nn_pytorch_flat(epoche=10,HIDDEN_DIM=10,cuda=True)
		x,y,death_functions=self.build_vectors(diz_cells,meta=death_functions)
		x=self.scaler.fit_transform(x)
		#x=self.pca.fit_transform(x)
		self.model.fit(x,y,death_functions,batch=500)
		return 
		
	def build_vectors(self,diz,meta=False):
		x=[]
		y=[]
		dafare=diz.keys()
		if meta:
			self.hashing,self.controashing=self.build_hashing(meta.keys())
		for i in dafare:
			x+=[diz[i]['features']]
			if meta:
				v=[0]*len(self.hashing.keys())
				v[self.hashing[diz[i]['cell_line']]]=1 # uno alla linea cellulare giusta
				y+=[v]
		if meta:
			popsurv=[]
			for i in sorted(self.controashing.keys()):
				popsurv+=[meta[self.controashing[i]]]
			return x,y,popsurv
		else:
			return x
			
	def build_hashing(self,cell_line_list):
		hashing={}
		controashing={}
		self.list_cell_lines=cell_line_list
		for i in range(len(cell_line_list)):
			hashing[cell_line_list[i]]=i
			controashing[i]=cell_line_list[i]
		return hashing,controashing
		
	def predict(self,diz_cells,co):
		x=self.build_vectors(diz_cells)
		x=self.scaler.transform(x)
		#x=self.pca.transform(x)
		return self.model.decision_function(x,co)

def crossvalida():
	blacklist=[]
	diz,cell_lines_list=parser.parse_featurea(return_cell_lines_list=True,toy=False)#'dataset/AMG_Gabriele.txt'
	death_functions=parser.parse_sigmoids()
	cell_type={}
	cell_lines=death_functions.keys()
	#blacklist=['BT320','BT333','LBT003','LBT059'] # 99 con questo?
	#blacklist=['BT320','LBT003']
	xtot=[]
	for i in blacklist:
		cell_lines.remove(i)
		
	for cl in cell_lines:
		print 'starting',cl
		### cancello la cell line in testing, controlla che non ci sia ###
		diz_train=diz.copy()
		diz_pred={}
		for i in cell_lines_list[cl]:
			diz_pred[i]=diz[i]
			del diz_train[i]
		meta_train=death_functions.copy()
		del meta_train[cl]
		#####################################################
		model=single_cell_renf()
		model.fit(diz_train,meta_train)
		
		
		concs=[-1.4082399653118496,-1.1072099696478683,-0.8061799739838872,-0.505149978319906,-0.2041199826559248,0.09691001300805642,0.3979400086720376,0.6989700043360189,1]
		ypred=[]
		for co in concs:
			ypred+=[model.predict(diz_pred,co)]
			print '\tscore:',np.mean(ypred),death_functions[cl].estimate(co)
		cell_type[cl]=ypred
	print 'done'
	cont=0
	xtot=[]
	ytot=[]
	labels=[]
	
	for i in cell_type.keys():
		x=[]
		y=[]
		for co in range(len(concs)):

			print i, np.mean(cell_type[i][co]),death_functions[i].estimate(concs[co])
			#print cell_type[i][200:300]
			x+=[np.mean(cell_type[i][co])]
			y+=[death_functions[i].estimate(concs[co])]
		labels+=[i]
		xtot+=[x]
		ytot+=[y]
		
	#for i in range(len(labels)):
	xtot=np.array(xtot)
	ytot=np.array(ytot)
	for co in range(len(concs)):		
			#print concs[co]
			x=xtot[:,co]
			y=ytot[:,co]
			#print '\tspearman',spearmanr(x,y)[0]
			print np.power(10,concs[co]),'\tpearson',pearsonr(x,y)[0],'sig',pearsonr(x,y)[1]
		
			fig, ax = plt.subplots()
			plt.title("NN - starting from Controls")
			
			ax.scatter(x,y)
			for i, txt in enumerate(labels):
				ax.annotate(txt, (x[i], y[i]))
			plt.text(0.2, 10,'Pearson: '+str(pearsonr(x,y)[0]) , fontdict=None, withdash=False)
			plt.xlabel('score')
			plt.ylabel('Survivability at '+str(np.power(10,concs[co])))
			plt.savefig('plots/rein_15_epochs_'+str(np.power(10,concs[co]))+'.png')
			plt.show()
def main(args):
	diz,cell_lines_list=parser.parse_featurea(return_cell_lines_list=True)#'dataset/AMG_Gabriele.txt'
	diz_blind,cl_blind=parser.parse_featurea('dataset/blinds/blinddata_Gabriele.txt',return_cell_lines_list=True)
	meta=parser.parse_sigmoids()
	sigmoid=parser.fsigmoid
	model=single_cell_renf()
	model.fit(diz,meta)
	for i in cl_blind.keys():
		d={}
		for k in cl_blind[i]:
			if diz_blind[k]['condition']!='CTRL':
				continue
			d[k]=diz_blind[k]
		ypred=model.predict(d)
		print i,np.mean(ypred)
	
		
if __name__ == '__main__':
    import sys
    crossvalida()
    sys.exit(main(sys.argv))
