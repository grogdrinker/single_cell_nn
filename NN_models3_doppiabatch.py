#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2017 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import torch
import sys
import torch.autograd as autograd
import torch.nn as nn
import torch.optim as optim
from torch.nn import  BCELoss
from torch.nn import MaxPool1d
import numpy as np
from torch.utils.data import TensorDataset,DataLoader
torch.manual_seed(1)
import math
import torch
from torch import nn
from torch.nn import init, Parameter
from torch.nn import functional as F
from torch.autograd import Variable


# Noisy linear layer with independent Gaussian noise
class NoisyLinear(nn.Linear):
  def __init__(self, in_features, out_features, sigma_init=0.017, bias=True):
    super(NoisyLinear, self).__init__(in_features, out_features, bias=True)  # TODO: Adapt for no bias
    # µ^w and µ^b reuse self.weight and self.bias
    self.sigma_init = sigma_init
    self.sigma_weight = Parameter(torch.Tensor(out_features, in_features))  # σ^w
    self.sigma_bias = Parameter(torch.Tensor(out_features))  # σ^b
    self.register_buffer('epsilon_weight', torch.zeros(out_features, in_features))
    self.register_buffer('epsilon_bias', torch.zeros(out_features))
    self.reset_parameters()

  def reset_parameters(self):
    if hasattr(self, 'sigma_weight'):  # Only init after all params added (otherwise super().__init__() fails)
      init.uniform(self.weight, -math.sqrt(3 / self.in_features), math.sqrt(3 / self.in_features))
      init.uniform(self.bias, -math.sqrt(3 / self.in_features), math.sqrt(3 / self.in_features))
      init.constant(self.sigma_weight, self.sigma_init)
      init.constant(self.sigma_bias, self.sigma_init)

  def forward(self, input):
    
    if self.training:
        return F.linear(input, self.weight + self.sigma_weight * Variable(self.epsilon_weight), self.bias + self.sigma_bias * Variable(self.epsilon_bias))
    else:
		return input

  def sample_noise(self):
    self.epsilon_weight = torch.randn(self.out_features, self.in_features)
    self.epsilon_bias = torch.randn(self.out_features)

  def remove_noise(self):
    self.epsilon_weight = torch.zeros(self.out_features, self.in_features)
    self.epsilon_bias = torch.zeros(self.out_features)

class GaussianNoise(nn.Module):
	"""Gaussian noise regularizer.

	Args:
		sigma (float, optional): relative standard deviation used to generate the
			noise. Relative means that it will be multiplied by the magnitude of
			the value your are adding the noise to. This means that sigma can be
			the same regardless of the scale of the vector.
		is_relative_detach (bool, optional): whether to detach the variable before
			computing the scale of the noise. If `False` then the scale of the noise
			won't be seen as a constant but something to optimize: this will bias the
			network to generate vectors with smaller values.
	"""

	def __init__(self, sigma=0.1, is_relative_detach=True,cuda=False):
		super(GaussianNoise,self).__init__()
		self.sigma = sigma
		self.is_relative_detach = is_relative_detach
		if not cuda:
			self.noise = torch.tensor(0)
		else:
			self.noise = torch.tensor(0).cuda()
		self.cuda=cuda
	def forward(self, x):
		if self.training and self.sigma != 0:
			scale = self.sigma * x.detach() if self.is_relative_detach else self.sigma * x
			if self.cuda:
				scale=scale.cuda()
			sampled_noise = self.noise.repeat(*x.size()).float().normal_() * scale
			if self.cuda:
				sampled_noise=sampled_noise.cuda()
			x = x + sampled_noise
		return x
class nn1(nn.Module):

	def __init__(self, n_features,hidden_dim=10,cuda=True):
		super(nn1, self).__init__()
		self.cuda=cuda
		#self.noise=GaussianNoise(cuda=self.cuda)
		#print n_features
		
		if self.cuda:
			self.net = nn.Sequential(
								nn.Linear(n_features, hidden_dim),
								nn.Sigmoid(),#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								nn.Linear(hidden_dim, hidden_dim),
								nn.Sigmoid(),#nn.ReLU(),
								nn.Linear(hidden_dim, hidden_dim),
								nn.Sigmoid(),#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								#nn.Linear(hidden_dim, hidden_dim),
								#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								#nn.Linear(hidden_dim, hidden_dim),
								#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								nn.Linear(hidden_dim,1),
								#nn.Sigmoid()
								).cuda()
		else:
			self.net = nn.Sequential(
								nn.Linear(n_features, hidden_dim),
								nn.Sigmoid(),#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								nn.Linear(hidden_dim, hidden_dim),
								nn.Sigmoid(),#nn.ReLU(),
								nn.Linear(hidden_dim, hidden_dim),
								
								nn.Sigmoid(),#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								#nn.Linear(hidden_dim, hidden_dim),
								#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								#nn.Linear(hidden_dim, hidden_dim),
								#nn.ReLU(),
								#nn.BatchNorm1d(hidden_dim),
								nn.Linear(hidden_dim,1),
								#nn.Sigmoid()
								)
		'''
		p=[]
		for i in self.net.parameters():
			print i.shape,i.view(-1).cpu().data.numpy().shape
			p+=[i.view(-1).cpu().data.numpy()]
		print len(p),hidden_dim
		ads
		'''
	def forward(self, sentence):
		
		##senza hidden
		#sentence=sentence.view(len(sentence), -1)
		#sentence=self.noise(sentence)
	
		out=self.net(sentence)
		
		return out
		#out=self.net(sentence)
		## con hidden
		'''
		sentence=sentence.view(len(sentence), -1)
		out=self.linear_hidden(sentence)
		out=self.sigmoid(out)
		#out=self.noise(out)
		out=self.linear(out)
		out=self.sigmoid(out)
		'''
		return out
class nn_pytorch_flat():
	def __init__(self,HIDDEN_DIM = 30,epoche=100,lr=0.001 ,weight_decay=1e-4,batch=5,cuda=False):
		self.epoche=epoche
		self.lr=lr
		self.weight_decay=weight_decay
		self.cuda=cuda
		self.HIDDEN_DIM=HIDDEN_DIM
		self.batch=batch
	def get_params(self,deep=False):
		return {}
	def load_model(self,model):
		self.model=torch.load(model,map_location={'cuda:0': 'cpu'})
	def fit(self,x,y,death_functions,verbose=2,name=None,batch=None):
		death_cell=death_functions[0]
		doppiabatch=50
		#pop_surv=np.array(pop_surv)
		concs=[-1.4082399653118496,-1.1072099696478683,-0.8061799739838872,-0.505149978319906,-0.2041199826559248,0.09691001300805642,0.3979400086720376,0.6989700043360189,1]
		concs=[-1.1072099696478683,-0.505149978319906,0.3979400086720376]
		pop_surv=[]
		for i in concs:
			t=[]
			for j in death_functions:
				t+=[j.estimate(i)]
			pop_surv+=[t]
		if self.cuda:
			pop_surv=autograd.Variable(torch.Tensor(pop_surv)).cuda()
		else:
			pop_surv=autograd.Variable(torch.Tensor(pop_surv))
		y=torch.Tensor(y).ge(0.5)
		assert len(y.shape)==2 # mask per ogni cell line
		if batch==None:
			self.batch=len(x) ### una volta sola
		else:
			self.batch=batch
		dataset=DataLoader(TensorDataset(torch.FloatTensor(x),y), batch_size=self.batch, shuffle=True, sampler=None, batch_sampler=None, num_workers=4, pin_memory=False, drop_last=False, timeout=0, worker_init_fn=None)
		model=nn1(n_features=len(x[0]), hidden_dim=self.HIDDEN_DIM,cuda=self.cuda)
		#model = gru1(n_features=len(x[0][0]), hidden_dim=self.HIDDEN_DIM,cuda=self.cuda)
		#optimizer = optim.SGD(model.parameters(), lr=self.lr, weight_decay=self.weight_decay)
		optimizer = optim.Adam(model.parameters())
		loss_function = nn.MSELoss()
		p=[]
		for i in  model.parameters():
			
			p+= list(i.data.cpu().view(-1).numpy())
		print 'number of parameters=',len(p)
		cont=1
		print 'epoche,tot ',self.epoche
		
		for epoch in range(self.epoche):
			verbose=2
			doppiabatch_cont=0
			auc=[]
			if verbose>=2 and epoch%1==0:
				sys.stdout.write('starting epoch'+str(epoch)+' ')
			loss_tot=[]
			model.zero_grad()
			exp_list=[]
			pre_list=[]
			for i_batch, sample_batched in enumerate(dataset):
				x_train,y_train=sample_batched
				if self.cuda:
					x_train = autograd.Variable(x_train).cuda() #prepare_sequence(sentence, word_to_ix)
					y_train = autograd.Variable(y_train).cuda() 
				else:
					x_train = autograd.Variable(x_train) #prepare_sequence(sentence, word_to_ix)
					y_train =  autograd.Variable(y_train)

				doppiabatch_cont+=1
				tag_scores = model(x_train).view(-1)
				trues=y_train
				preds=tag_scores
				'''
				mask = trues.ge(0.5)
				preds_pos=torch.masked_select(preds, mask)
				true_pos=torch.masked_select(trues, mask)
				mask_neg=(trues*-1).ge(-0.5)
				preds_neg=torch.masked_select(preds, mask_neg)
				true_neg=torch.masked_select(trues, mask_neg)
				'''
				#rate=len(torch.nonzero(trues.data))/float(len(trues.data))
				tmp_exp=[]
				tmp_pre=[]
				sigmoid=nn.Sigmoid()
				for co in xrange(pop_surv.shape[0]):
					if self.cuda:
						expected= autograd.Variable(torch.Tensor([0.0])).cuda()
						#predicted=autograd.Variable(torch.Tensor([0.0])).cuda()
					
					else:
						expected=autograd.Variable(torch.Tensor([0.0]))
						#predicted=autograd.Variable(torch.Tensor([0.0]))
					for i in xrange(pop_surv.shape[1]):
						#masked=torch.masked_select(preds,trues[:,i])
						#print masked
						expected= torch.add(torch.mul(torch.sum(trues[:,i]),pop_surv[co][i]),expected )	
					#print 'composizione'
					#for i in xrange(pop_surv.shape[0]):
					#	print '\t',torch.sum(trues[:,i]).cpu().data.numpy()
					preds=sigmoid(preds-co)
					predicted=torch.div(torch.sum(preds),x_train.shape[0])
					if torch.isnan(torch.sum(preds)).cpu().data.numpy():
						print preds
						dfgg
					expected =torch.div(expected,x_train.shape[0])
					tmp_exp+=[expected.view(1,1)]
					tmp_pre+=[predicted.view(1,1)]
				exp_list+=[torch.cat(tmp_exp)]
				pre_list+=[torch.cat(tmp_pre)]

				if doppiabatch_cont%doppiabatch==0 :
					loss=[]
					pr_fin=torch.cat(pre_list,1)
					ex_fin=torch.cat(exp_list,1)
					for i in range(ex_fin.shape[0]):
						vx = pr_fin - torch.mean(pr_fin[i])
						vy = ex_fin - torch.mean(ex_fin[i])
						if torch.sum(torch.isnan(vx)).cpu().data.numpy()>0.5:
							print pr_fin
							print ex_fin
							asd
							
						#print ((torch.sqrt(torch.sum(vx ** 2)) * torch.sqrt(torch.sum(vy ** 2)))+0.00001)
						cost = torch.sum(vx * vy) / ((torch.sqrt(torch.sum(vx ** 2)) * torch.sqrt(torch.sum(vy ** 2)))+0.00001)
						loss+=[cost.view(1,1)+loss_function(pr_fin[i],ex_fin[i])]
					loss=torch.mean(torch.cat(loss))
					loss.backward()
					optimizer.step()
					model.zero_grad()
					exp_list=[]
					pre_list=[]
					loss_tot+=[float(loss.cpu().data.numpy().flat[0])]
			if verbose>=2 and epoch%1==0:
				sys.stdout.write( ' '+str(np.mean(loss_tot))+'\n')
			#if np.mean(loss_tot)<0.045:
			#	break
		self.model=model
		#torch.save(self.model,'models/prova_flatmodel.m')
		#precheck_sent = prepare_sequence(training_data[0][0], word_to_ix)
	def decision_function(self,x,concentration):
		y_pred=[]
		#x = [item for sublist in X for item in sublist]
		#print np.
		sigmoid=nn.Sigmoid()
		if self.cuda:
			y_pred=self.model(autograd.Variable(torch.Tensor(x).cuda()))
			y_pred=list(sigmoid(concentration-y_pred).data.cpu().numpy().flat)
		else:
			y_pred=self.model(autograd.Variable(torch.Tensor(x)))
			y_pred=list(sigmoid(concentration-y_pred).data.numpy().flat)
		assert len(y_pred)==len(x)
			
		return y_pred#y_pred
def false_auc(output, target, weights=None,perc=0.3):
	tieni=int((len(output)*perc)/2)
	scores, sortind = torch.sort(output, dim=0, descending=True)
	sorted_target=target[sortind]
	mask_true=sorted_target.ge(0.5)
	masked_out_true=torch.masked_select(scores,mask_true)
	mask_false=(-1*sorted_target).ge(-0.5)
	masked_out_false=torch.masked_select(scores,mask_false)
	#print masked_out_false[-tieni:]
	return torch.mean(masked_out_false[:])-torch.mean(masked_out_true[:])
def weighted_binary_cross_entropy(output, target, weights=None):
        
    if weights is not None:
        assert len(weights) == 2
        
        loss = weights[1] * (target * torch.log(output)) + \
               weights[0] * ((1 - target) * torch.log(1 - output))
    else:
        loss = target * torch.log(output) + (1 - target) * torch.log(1 - output)

    return torch.neg(torch.mean(loss))
if __name__ == '__main__':
	
		
		x=[[[1,0,1],[1,0,0],[1,1,1],[0,0,0],[1,0,1],[1,0,0],[1,1,1],[0,0,0],[1,0,1],[1,0,0]],[[1,1,1],[0,0,0],[1,0,1],[1,0,0],[1,1,1],[0,0,0],[1,0,1],[1,0,0],[1,1,1],[0,0,0],[1,0,1],[1,0,0],[1,1,1],[0,0,0]]]
		y=[[1,0]]*15+[[0,1]]*9
		x = [item for sublist in x for item in sublist]
		cell_lines=[0.7,0.1]
		#y = [item for sublist in y for item in sublist]
		ss=[[0.5,0.5,0.5],[0.5,0.5,0.5],[0.5,0.5,0.5]]
		c=torch.FloatTensor(ss).cuda()
		f=nn1(3,cuda=True)
		a=nn_pytorch_flat(epoche=3000,cuda=True)
		a.fit(x,y,cell_lines,batch=3)
		print a.decision_function(x)
