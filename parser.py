#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  
#  Copyright 2019 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
import matplotlib.pylab as plt
import scipy
from scipy.optimize import curve_fit
def parse_featurea(fil='dataset/Control_Gabriele.txt',return_cell_lines_list=False,toy=False):
	header=True
	cont=0
	diz={}
	cell_lines_list={}
	blacklist=["Viability_2","Viability_1","Cisplatin"]
	if not toy:
		dafare=open(fil).readlines()
	else:
		
		dafare=open(fil).readlines()
		dafare=[dafare[0]]+list(np.random.choice(dafare,55000))
	for lin in dafare:
		if header:
			h=lin.strip().replace('"','').split()
			header=False
		else:
			a=lin.strip().replace('"','').split()
			fea=[]
			cond=False
			for i in range(len(h)):
				if h[i] in blacklist:
					continue
				if h[i]=='sample':
					continue
				elif h[i]=='PDCLS' or h[i]=='Sample_ID':
					cell=a[i]
				elif h[i]=='Condition':
					cond=a[i]
				else:
					fea+=[float(a[i])]
			diz[str(cont)]={}
			diz[str(cont)]["cell_line"]=cell
			diz[str(cont)]["features"]=fea
			diz[str(cont)]["condition"]=cond
			if not cell_lines_list.has_key(cell):
				cell_lines_list[cell]=[]
			cell_lines_list[cell]+=[str(cont)]
			cont+=1
	if not return_cell_lines_list:
		return diz
	else:
		return diz,cell_lines_list
class fsigmoid:
	def __init__(self):
		pass
	def scipy_fit(self, x, ec50,top):
		return top / (1.0 + np.power(10,(-(x-ec50))))
	def fit(self,ec50,top):
		self.ec50=ec50
		self.top=top
	def estimate(self,x):
		return self.top / (1.0 + np.power(10,(-(x-self.ec50))))
def parse_sigmoids(fil='dataset/surv.txt'):
	final={}
	nome=False
	for l in open(fil,'r').readlines():
		if l[0]=='#':
			if nome:
				e=[]
				
				#### normalizziamo ###
				for day in range(len(final[nome][final[nome].keys()[0]])):
					mat=[]
					for conc in sorted(final[nome].keys()):
	
						#print day,conc,final[nome][conc][day]
						mat+=[final[nome][conc][day]]
					mat=np.array(mat)
					for i in range(mat.shape[1]):
						mat[:,i]/=max(mat[:,i])
					e+=[np.mean(mat,axis=1)]
				e=np.array(e)
				
				c=sorted(final[nome].keys())
				if c[0]<=0:
					c[0]=0.0001
				x=[]
				y=[]
				for i in range(mat.shape[0]):
					for j in range(mat.shape[1]):
						x+=[np.log(c[i])]
						y+=[1-mat[i][j]]

				final[nome]=(x,y)

		
			nome=l[1:].strip()
			if nome!='':
				final[nome]={}
		elif l=='\n':
			continue
		else:
			a=l.split()
			conc=float(a[0])
			exps=[]
			repeats=[]
			for i in a[1:]:
				if i=='-':
					exps+=[repeats]
					
					repeats=[]
				else:
					repeats+=[float(i)]
			if repeats!=[]:
				exps+=[repeats]
				repeats=[]
			final[nome][conc]=np.array(exps)
	final_vero={}
	for nome in final.keys():
		#print final[nome]
		
		a=fsigmoid()
		popt, pcov = curve_fit(a.scipy_fit, final[nome][0],final[nome][1], method='trf',bounds=[(-10,0),(10,1)])
		a.fit(popt[0],popt[1])
		final_vero[nome]=a
	return final_vero
	if True:
		#jj=fsigmoid()
		x = np.linspace(-10, 10, 50)
		y = a.estimate(x)
		plt.plot(x,y)
		plt.ylim(0,1)
		plt.plot(final[nome][0],final[nome][1],'ro')
		plt.xlabel('LOG( [Drug] )')
		plt.ylabel('Fraction of killed cells')
		plt.show()
	#'''
def read_metadata(fil='dataset/metadata_gabriele.txt'):
	header=True
	cont=0
	diz={}
	for lin in open(fil).readlines():
		if header:
			h=lin.strip().replace('"','').split()
			header=False
		else:
		
			a=lin.strip().replace('"','').split()
			fea=[]
			for i in range(len(h)):
				if h[i]=='PDCLS':
					
					name=a[i]
				elif h[i]=='AUC':
					auc=float(a[i])
				else:
					continue
			diz[name]=auc
	return diz
def read_sopravvivenza_tuo(fil='dataset/sopravvivenza2.5_tuo'):
	diz={}
	for i in open(fil):
		diz[i.split()[0].upper()]=float(i.split()[1])/100
	return diz
def main(args):
	#print parse_featurea()['54']
	#print read_sopravvivenza_tuo()
	print parse_sigmoids()
if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
